# 用户管理

## 数据库

使用Firestore Database存储数据，替代mysql
使用Realtime Database替代redis
使用Storage替代CDN

## 用户管理

使用CustomToken实现

由于前端可以自行调用服务平台登录，故此服务不开放服务平台管理功能(但是有相关服务)

uid用于关联服务账号和管理账号，email和phone作为登录凭证(只实现了email/password)

jwt是可以相互验证的，但通过manage是否为true来判断能否用于管理平台(管理平台的jwt可以在服务平台通用, 同时退出管理平台服务平台也会退出)

这些令牌会在 1 小时后过期

创建管理账户方式

1. 初始化：如果不存在管理账户，将引导创建一个root账户
2. 新增：只能使用邀请制，可以选择权限过继(TODO)

邮件校验由前端来实现，后端只接收是否通过邮件校验参数

### 权限设计

#### Authority

- *: 通用权限，无限制
- del_user: 删除用户，范围看role
- invite_user: 邀请用户注册
- messaging: 消息通告，视role定范围

#### Role

- root
- admin
- group_manager: 限制于自己组
- user
