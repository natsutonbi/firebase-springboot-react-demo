package com.example.demo.config;

import java.io.FileInputStream;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.example.demo.config.exception.FirebaseInitException;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.Firestore;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.database.FirebaseDatabase;
// import com.google.firebase.messaging.FirebaseMessaging;

@Configuration
public class FirebaseConfig {
    // public FirebaseApp app = null;

    // @PostConstruct
    // void init() throws FirebaseInitException{
    //     try {
    //         // Resource resource = resourceLoader.getResource("classpath:config/firebase.json");
    //         // FileInputStream serviceAccount =
    //         //     new FileInputStream(resource.getFile());
    //         GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();// on google cloud
    //         // GoogleCredentials credentials2 = GoogleCredentials.fromStream(serviceAccount);// on other server
    //         FirebaseOptions options = FirebaseOptions.builder()
    //             .setCredentials(credentials)
    //             .setProjectId("springboot-demo")
    //             .build();
    //         if (FirebaseApp.getApps().isEmpty()) {
    //             app = FirebaseApp.initializeApp(options);
    //             // FirebaseApp.initializeApp();
    //         }
    //     } catch (Exception e) {
    //         throw new FirebaseInitException("Firebase初始化失败: "+e.getMessage(), e.getCause());
    //     }
    // }

    @Bean
    GoogleCredentials googleCredentials(ResourceLoader resourceLoader) throws FirebaseInitException {// 要么从文件读，要么自动加载
        try {
            Resource resource = resourceLoader.getResource("classpath:config/firebase.json");
            FileInputStream serviceAccount =
                new FileInputStream(resource.getFile());
            return GoogleCredentials.fromStream(serviceAccount);
        } catch (Exception e) {
            try {
                // Use standard credentials chain. Useful when running inside GKE
                return GoogleCredentials.getApplicationDefault();
            } catch (Exception ee) {
                throw new FirebaseInitException("Firebase初始化失败, 尝试从文件中读取失败: "+e.getMessage() + ", 同时加载default失败: "+ee.getMessage());
            }
        }
    }

    @Bean
    FirebaseApp firebaseApp(GoogleCredentials credentials) {
        FirebaseOptions options = FirebaseOptions.builder()
        .setCredentials(credentials)
        .build();

        return FirebaseApp.initializeApp(options);
    }

    @Bean
	public Firestore firestore(FirebaseApp app) {
		return FirestoreClient.getFirestore(app);
	}

    @Bean
    public FirebaseDatabase database(FirebaseApp app){
        return FirebaseDatabase.getInstance(app);
    }

    @Bean
    public FirebaseAuth auth(FirebaseApp app){
        return FirebaseAuth.getInstance(app);
    }

    // @Bean
    // public FirebaseMessaging firebaseMessaging(FirebaseApp app){ // firebase cloud messaging; 消息队列
    //     return FirebaseMessaging.getInstance(app);
    // }

    @Bean(name = "manage")
    public CollectionReference manageAccountRef(FirebaseApp app){
        return FirestoreClient.getFirestore(app).collection("manager");
    }
}
