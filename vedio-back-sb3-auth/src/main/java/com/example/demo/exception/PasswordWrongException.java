package com.example.demo.exception;

import org.springframework.security.core.AuthenticationException;

public class PasswordWrongException extends AuthenticationException{

    public PasswordWrongException() {
        super("wrong password");
    }

    public PasswordWrongException(String msg) {
        super(msg);
    }
    
}
