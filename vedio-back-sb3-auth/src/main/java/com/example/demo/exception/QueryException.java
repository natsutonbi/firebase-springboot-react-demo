package com.example.demo.exception;

public class QueryException extends FirebaseAccessException{
    public QueryException(String msg){
        super(msg);
    }

    public QueryException(){
        super("server internal error, query failed");
    }
}
