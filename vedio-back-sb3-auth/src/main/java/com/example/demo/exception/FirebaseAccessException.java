package com.example.demo.exception;

public class FirebaseAccessException extends Exception{
    public FirebaseAccessException(String msg){
        super("error happened while accessing database, this message might help: " + msg);
    }

    public FirebaseAccessException(){
        super("error happened while accessing database");
    }
}
