package com.example.demo.exception;

public class EmptyInputException extends Exception {
    public EmptyInputException() {
        super("empty input");
    }

    public EmptyInputException(String type) {
        super(type + " can't be empty");
    }
}
