package com.example.demo.security.entity.vo.request;

import lombok.Data;

@Data
public class EmailMessageVO {
    String reciever_email;
    boolean on_manage;
    String reciever_uid;
    String content;
}
