package com.example.demo.security.filter;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.example.demo.security.entity.dao.ManageAccount;
import com.example.demo.security.entity.dto.MyUser;
import com.example.demo.security.service.FirebaseJwtService;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/*
 * 通过jwt验证
 */
@Component
public class JwtAuthorizeFilter extends OncePerRequestFilter {// 每次请求

    @Autowired
    FirebaseJwtService firebaseJwtService;

    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain)
            throws ServletException, IOException {
        String rawJwt = request.getHeader("Authorization");
        if(rawJwt.startsWith("Bearer ")){
            rawJwt = rawJwt.substring(7);
            if(firebaseJwtService.checkJwt(rawJwt)){
                ManageAccount manageAccount = firebaseJwtService.getManageAccount(rawJwt);
                MyUser user = new MyUser(manageAccount);
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                    user, null, user.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContext context = SecurityContextHolder.createEmptyContext();
                context.setAuthentication(authenticationToken);
                SecurityContextHolder.setContext(context);
            }
        }
        filterChain.doFilter(request, response);
    }

}
