package com.example.demo.security.entity.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ServiceAccount {
    String uid; // 自动生成
    String email;
    boolean emailVerified;
    String password; //注册时需要
    String phone;
    String displayName;
    String photoUrl;
    boolean disabled;
}
