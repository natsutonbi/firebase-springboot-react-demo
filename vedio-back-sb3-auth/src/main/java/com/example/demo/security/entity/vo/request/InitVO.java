package com.example.demo.security.entity.vo.request;

import jakarta.validation.constraints.Email;
import lombok.Data;

@Data
public class InitVO {
    String rawPassword;
    String uid;
    
    @Email
    String email;
}
