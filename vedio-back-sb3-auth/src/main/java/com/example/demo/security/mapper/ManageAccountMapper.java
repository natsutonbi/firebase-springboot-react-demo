package com.example.demo.security.mapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.exception.EmptyInputException;
import com.example.demo.exception.QueryException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.security.entity.dao.ManageAccount;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class ManageAccountMapper {//调用Firestore
    @Autowired
    CollectionReference manageAccountRef;

    @Autowired
    PasswordEncoder passwordEncoder;

    String getDocName(ManageAccount account)throws EmptyInputException{
        if(account == null)
            throw new EmptyInputException("ManageAccount");
        String username = account.getNickname(), uid = account.getUid();
        if(username == null || uid == null)
            throw new EmptyInputException("username or uid of ManageAccount");
        return username + "@" + uid;
    }

    DocumentReference getDoc(ManageAccount account)throws EmptyInputException{
        String docName = getDocName(account);
        if(docName == null)
            throw new EmptyInputException("docName");
        return manageAccountRef.document(docName);
    }

    public boolean isEmpty() throws QueryException{
        // asynchronously retrieve all documents
        ApiFuture<QuerySnapshot> future = manageAccountRef.get();
        try {
            // future.get() blocks on response
            List<QueryDocumentSnapshot> documents = future.get().getDocuments();
            return documents.isEmpty();
        } catch (Exception e) {
            throw new QueryException();
        }
    }

    public void addAccount(ManageAccount account) throws EmptyInputException{
        if(account == null) throw new EmptyInputException("manage account");
        
        DocumentReference docRef = getDoc(account);
        ApiFuture<WriteResult> result = docRef.set(account);
        try {
            log.info("successfully add account<{}> at ", account.getNickname(), result.get().getUpdateTime());
        } catch (Exception e) {
            log.error("failed to add account<{}>", account.getNickname());
            return;
        }
    }

    public void deleteAccount(ManageAccount account) throws EmptyInputException{
        try {
            log.info("successfully add account<{}> at ", getDocName(account), getDoc(account).delete().get().getUpdateTime());
        } catch (Exception e) {
            log.error("failed to del account<{}>", getDocName(account));
        }
    }

    public void updateAccount(ManageAccount account) throws EmptyInputException{
        if(account == null) return;
        getDoc(account).set(account);
    }

    public ManageAccount getAccountByEmail(String email) throws UserNotFoundException, QueryException, EmptyInputException {
        if(email == null) throw new EmptyInputException("email of manager");
        Query query = manageAccountRef.whereEqualTo("email", email).limit(1);
        QuerySnapshot querySnapshot;
        try {
            querySnapshot = query.get().get();
        } catch (Exception e) {
            throw new QueryException("query failed while finding account by email " + email);
        }
        if (querySnapshot.isEmpty()) {
            throw new UserNotFoundException("email<" + email + "> doesn't exist");
        }
        QueryDocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
        return documentSnapshot.toObject(ManageAccount.class);
    }

    public ManageAccount getAccountByUid(String uid) throws UserNotFoundException, QueryException, EmptyInputException {
        if(uid == null) throw new EmptyInputException("uid of manager");
        Query query = manageAccountRef.whereEqualTo("uid", uid).limit(1);
        QuerySnapshot querySnapshot;
        try {
            querySnapshot = query.get().get();
        } catch (Exception e) {
            throw new QueryException("query failed while finding account by uid " + uid);
        }
        if (querySnapshot.isEmpty()) {
            throw new UserNotFoundException("uid<" + uid + "> doesn't exist");
        }
        QueryDocumentSnapshot documentSnapshot = querySnapshot.getDocuments().get(0);
        return documentSnapshot.toObject(ManageAccount.class);
    }

    public boolean existEmail(String email) {
        try {
            getAccountByEmail(email);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
