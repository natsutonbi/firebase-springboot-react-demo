package com.example.demo.security.entity.vo.request;

import jakarta.validation.constraints.Email;
import lombok.Data;

@Data
public class InviteVO {
    String uid;
    String rawPassword;
    String nickname;

    @Email
    String email;
}
