package com.example.demo.security.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.security.entity.dao.ServiceAccount;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.auth.UserRecord.CreateRequest;
import com.google.firebase.auth.UserRecord.UpdateRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ServiceAccountMapper {// 调用Authentication

    @Autowired
    FirebaseAuth auth;

    ServiceAccount userRecord2ServiceAccount(UserRecord record){
        return new ServiceAccount()
                .setUid(record.getUid())
                .setDisabled(record.isDisabled())
                .setDisplayName(record.getDisplayName())
                .setEmail(record.getEmail())
                .setEmailVerified(record.isEmailVerified())
                .setPhone(record.getPhoneNumber())
                .setPhotoUrl(record.getPhotoUrl());
    }

    UpdateRequest serviceAccount2UpdateRequest(ServiceAccount account){
        return new UpdateRequest(account.getUid())
            .setEmail(account.getEmail())
            .setEmailVerified(account.isEmailVerified())
            .setPassword(null) //需显式设置
            .setPhoneNumber(account.getPhone())
            .setDisplayName(account.getDisplayName())
            .setDisabled(account.isDisabled());
    }

    public String getUidByEmail(String email){
        try {
            UserRecord record = auth.getUserByEmail(email);
            return record.getUid();
        } catch (FirebaseAuthException e) {
            log.error(e.getMessage(), e.getCause());
            return null;
        }
    }

    public ServiceAccount getServiceAccountByUid(String uid){
        try {
            UserRecord record = auth.getUser(uid);
            return userRecord2ServiceAccount(record);
        } catch (FirebaseAuthException e) {
            log.error("user not found");
            log.error(e.getMessage(), e.getCause());
            return null;
        }
    }

    public ServiceAccount getServiceAccountByEmail(String emial){
        try {
            UserRecord record = auth.getUserByEmail(emial);
            return userRecord2ServiceAccount(record);
        } catch (FirebaseAuthException e) {
            log.error("user not found");
            log.error(e.getMessage(), e.getCause());
            return null;
        }
    }

    public ServiceAccount getServiceAccountByPhone(String phone){
        try {
            UserRecord record = auth.getUserByPhoneNumber(phone);
            return userRecord2ServiceAccount(record);
        } catch (FirebaseAuthException e) {
            log.error("user not found");
            log.error(e.getMessage(), e.getCause());
            return null;
        }
    }

    public void updateServiceAccount(ServiceAccount account){
        try {
            auth.updateUser(serviceAccount2UpdateRequest(account));
            log.info("Successfully updated service account: " + account.getUid());
        } catch (Exception e) {
            log.error("Fail to updated service account: " + account.getUid());
            log.error(e.getMessage(), e.getCause());
        }
    }

    public void ChangeServiceAccountPassword(String uid, String password){ // 在已登录管理账号的情况下无条件修改
        UpdateRequest request = new UpdateRequest(uid)
            .setPassword(password);
        try {
            auth.updateUser(request);
            log.info("Successfully updated user's password: " + uid);
        } catch (Exception e) {
            log.error("Fail to updated user's password: " + uid);
            log.error(e.getMessage(), e.getCause());
        }
    }

    public void registServiceAccount(ServiceAccount account){
        CreateRequest request = new CreateRequest()
            .setEmail(account.getEmail())
            .setEmailVerified(account.isEmailVerified())
            .setPassword(account.getPassword())
            .setPhoneNumber(account.getPhone())
            .setDisplayName(account.getDisplayName())
            .setDisabled(account.isDisabled());
        UserRecord record;
        try {
            record = auth.createUser(request);
        } catch (FirebaseAuthException e) {
            log.error(e.getMessage(), e.getCause());
            return;
        }
        log.info("Successfully created new user: " + record.getUid());
    }

    public void delServiceAccount(String uid){
        try {
            auth.deleteUser(uid);
            log.info("Successfully deleted user<{}>.", uid);
        } catch (Exception e) {
            log.error("Failed to delete user<{}>.", uid);
        }
    }
}
