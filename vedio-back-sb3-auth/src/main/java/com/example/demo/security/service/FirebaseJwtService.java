package com.example.demo.security.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

// import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.exception.FirebaseAccessException;
import com.example.demo.security.entity.dao.ManageAccount;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FirebaseJwtService {

    @Autowired
    FirebaseAuth auth;

    @Autowired
    FirebaseDatabase database;

    public String createJwt(ManageAccount manageAccount) {
        Map<String, Object> additional = new HashMap<>();

        additional.put("username", manageAccount.getNickname());
        additional.put("roles", String.join(",", (manageAccount.getRoles())));
        additional.put("permissions", String.join(",", manageAccount.getPermissions()));
        additional.put("manage", true); //表明管理平台专用

        try {
            return auth.createCustomToken(manageAccount.getUid(), additional);
        } catch (FirebaseAuthException e) {
            log.error(e.getMessage(), e.getCause());
        }
        return null;
    }

    

    public boolean checkJwt(String rawJwt) {
        try {
            FirebaseToken decodedToken = auth.verifyIdToken(rawJwt, true);//检查revoked, revokeRefresh后立即阻止用户访问系统
            if((boolean)decodedToken.getClaims().get("manage"))//检查是否在manage平台
                return true;
            else
                return false;
        } catch (FirebaseAuthException e) {
            log.error(e.getMessage(), e.getCause());
            return false;
        }
    }

    public ManageAccount getManageAccount(String rawJwt) {
        try {
            FirebaseToken decodedToken = auth.verifyIdToken(rawJwt);
            Map<String, Object> additional = decodedToken.getClaims();
            ManageAccount manageAccount = new ManageAccount()
                    .setUid(decodedToken.getUid())
                    .setNickname(((String) additional.get("username")))
                    .setRoles(
                            new ArrayList<String>(Arrays.asList(((String) additional.get("roles")).split(","))))
                    .setPermissions(
                            new ArrayList<String>(Arrays.asList(((String) additional.get("permissions")).split(","))));
            return manageAccount;
        } catch (FirebaseAuthException e) {
            log.error(e.getMessage(), e.getCause());
            return null;
        }
    }

    public void revokeRefreshJwt(String uid)throws FirebaseAccessException{
        try {
            auth.revokeRefreshTokens(uid);// 禁止之前的jwt重新刷新
            long timestamp = auth.getUser(uid).getTokensValidAfterTimestamp();
            // 将时间戳转换为 Instant 对象
            Instant instant = Instant.ofEpochMilli(timestamp);
            // 将 Instant 对象转换为 LocalDateTime 对象
            LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
            log.info("Tokens revoked at: "+ dateTime);

            DatabaseReference ref = database.getReference("metadata/" + uid);
            Map<String, Object> userData = new HashMap<>();
            userData.put("revokeTime", timestamp/1000);
            ref.setValueAsync(userData);
        } catch (Exception e) {
            log.error("failed to revoke");
            throw new FirebaseAccessException(e.getMessage());
        }
    }
}
