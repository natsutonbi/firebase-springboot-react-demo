package com.example.demo.security.entity.dao;

import java.util.ArrayList;
import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ManageAccount { // 管理平台账号，uid与服务平台保持一致，会检查服务平台账号是否创建
    // documentId: username@uid  eg:root@PWfthotEUwSpIS8BxPOgJvPpG8L2
    String uid; //PWfthotEUwSpIS8BxPOgJvPpG8L2
    String nickname;
    String password;
    String email;

    ArrayList<String> roles = new ArrayList<>();
    ArrayList<String> permissions = new ArrayList<>();

    public ManageAccount addRoles(String... roles){
        this.roles.addAll(Arrays.asList(roles));
        return this;
    }

    public ManageAccount addPermissions(String... permissions){
        this.permissions.addAll(Arrays.asList(permissions));
        return this;
    }
}
