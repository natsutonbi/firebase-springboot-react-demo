package com.example.demo.security.service;

import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.exception.EmptyInputException;
import com.example.demo.exception.QueryException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.security.config.securityConfig;
import com.example.demo.security.entity.dao.ManageAccount;
import com.example.demo.security.entity.dto.MyUser;
import com.example.demo.security.mapper.ManageAccountMapper;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class MyUserManager implements UserDetailsService {

    static final String rolePrefix = securityConfig.rolePrefix;

    @Autowired
    ManageAccountMapper manageAccountMapper;

    @Autowired
    PasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String uid) throws UsernameNotFoundException {
        ManageAccount account;
        try {
            account = manageAccountMapper.getAccountByUid(uid);
        } catch (Exception e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
        return new MyUser(account);
    }

    public UserDetails loadUserByEmail(String email) throws UsernameNotFoundException {
        ManageAccount account;
        try {
            account = manageAccountMapper.getAccountByEmail(email);
        } catch (Exception e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
        return new MyUser(account);
    }

    // @Transactional(rollbackFor = Exception.class)
    public void createUser(MyUser myUser) throws EmptyInputException{
        ManageAccount account = myUser.getAccount();
        manageAccountMapper.addAccount(account);
    }

    /*
    * 密码不能通过这种方法修改
    */
    // @Transactional(rollbackFor = Exception.class)
    public void updateUser(MyUser updateInfo) throws UserNotFoundException, QueryException, EmptyInputException{ // 查一遍后对比
        if (updateInfo == null)
            return;

        ManageAccount updateAccount = updateInfo.getAccount(), oldAccount;
        
        if (updateAccount == null) {// MyUser的newAccount不可能为null
            return;
        }

        oldAccount = manageAccountMapper.getAccountByUid(updateAccount.getUid());
        updateAccount.setPassword(oldAccount.getPassword()); 
        manageAccountMapper.updateAccount(updateAccount);
    }

    public void changePassword(String uid, String oldPassword, String newPassword) throws UserNotFoundException {
        if (StringUtils.isEmpty(oldPassword) || StringUtils.isEmpty(newPassword)) {
            throw new IllegalArgumentException("旧密码或新密码不能为空");
        }
        ManageAccount oldAccount;
        try {
            oldAccount = manageAccountMapper.getAccountByUid(uid);
            if (encoder.matches(oldPassword, oldAccount.getPassword())) {
                oldAccount.setPassword(encoder.encode(newPassword));
                manageAccountMapper.updateAccount(oldAccount);
            } else{
                throw new BadCredentialsException("旧密码验证失败");
            }
        } catch (Exception e) {
            throw new UserNotFoundException(e.getMessage());
        }
    }

    @PreAuthorize("hasAuthority('del_user') or hasRole('root') or hasAuthority('*')")
    public void deleteUser(String uid)throws Exception{
        manageAccountMapper.deleteAccount(manageAccountMapper.getAccountByUid(uid));
    }
}
