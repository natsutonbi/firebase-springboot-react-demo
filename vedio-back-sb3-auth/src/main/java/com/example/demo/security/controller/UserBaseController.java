package com.example.demo.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.security.entity.dao.ManageAccount;
import com.example.demo.security.entity.dao.ServiceAccount;
import com.example.demo.security.entity.dto.MyUser;
import com.example.demo.security.entity.vo.request.InitVO;
import com.example.demo.security.entity.vo.request.InviteVO;
import com.example.demo.security.mapper.ManageAccountMapper;
import com.example.demo.security.mapper.ServiceAccountMapper;
import com.example.demo.security.service.FirebaseJwtService;
import com.example.demo.security.service.MailService;
import com.example.demo.security.service.MyUserManager;
import com.example.demo.utils.RestBean;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;


@RestController
@RequestMapping("/api/user")
@Tag(name = "用户基础接口", description = "注册/jwt续签/注销") //注销不需要写出; 续签前端就能自动完成
public class UserBaseController {

    @Autowired
    MyUserManager manager;

    @Autowired
    FirebaseJwtService firebaseJwtService;

    @Autowired
    ManageAccountMapper manageAccountMapper;

    @Autowired
    ServiceAccountMapper serviceAccountMapper;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    MailService mailService;

    @PostMapping("/init")
    public RestBean<String> postMethodName(@RequestBody InitVO initVO) {//要求在服务平台上有账户
        try {
            if(!manageAccountMapper.isEmpty()){
                return RestBean.failure(400, "用户数据库不为空，请用已有账户登录或者检查数据库");
            }
            ManageAccount account = new ManageAccount()
                .setUid(initVO.getUid())
                .setEmail(initVO.getEmail())
                .setNickname("root")
                .setPassword(encoder.encode(initVO.getRawPassword()))
                .addRoles("root")
                .addPermissions("*");
            manageAccountMapper.addAccount(account);
        } catch (Exception e) {
            return RestBean.failure(500, e.getMessage());
        }
        return RestBean.success("管理平台初始化成功，创建root用户成功");
    }

    @PreAuthorize("hasAuthority('invite_user') or hasAnyRole('root','admin') or hasAuthority('*')")
    @PostMapping("/regist/invite")
    public RestBean<String> postMethodName(@RequestBody InviteVO inviteVO) {
        String uid = inviteVO.getUid();
        ServiceAccount serviceAccount = serviceAccountMapper.getServiceAccountByUid(uid);//验证服务平台是否有账户
        if(serviceAccount == null){
            return RestBean.failure(400, "no such user on service: "+uid);
        }
        try {
            ManageAccount account = new ManageAccount()
                    .setUid(uid)
                    .setEmail(inviteVO.getEmail())
                    .setNickname(inviteVO.getNickname())
                    .setPassword(encoder.encode(inviteVO.getRawPassword()));
            manageAccountMapper.addAccount(account);
        } catch (Exception e) {
            return RestBean.failure(500, e.getMessage());
        }
        return RestBean.success();
    }

    @GetMapping("/refresh")
    public RestBean<String> getMethodName(Authentication authentication) {
        MyUser user = (MyUser) authentication.getPrincipal();
        return RestBean.success(firebaseJwtService.createJwt(user.getAccount()));
    }

    // @PostMapping("/regist/simple")
    // public RestBean<String> regist(@RequestParam("password") String password) {
    //     String username = manager.getNewUsername();
    //     Account account = new Account()
    //             .setUsername(username)
    //             .setPassword(encoder.encode(password))
    //             .setEnabled(true);
    //     MyUser user = new MyUser(account).addRole("USER");
    //     manager.createUser(user);
    //     return RestBean.success("你的用户id为" + username);
    // }
}
