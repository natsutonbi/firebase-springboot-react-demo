package com.example.demo;

import java.net.InetAddress;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.security.service.MyUserManager;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;

@RunWith(SpringRunner.class)
@SpringBootTest
public class testFirebase {
    
    @Autowired
    MyUserManager userManager;

    @Test
    public void pingGoogle() throws Exception{
        String host = "www.google.com"; // 要检查的主机名

        InetAddress inetAddress = InetAddress.getByName(host);

        // 调用 isReachable 方法来检查主机的连通性，超时时间为 1000 毫秒
        boolean reachable = inetAddress.isReachable(1000);

        if (reachable) {
            System.out.println("Host " + host + " is reachable.");
        } else {
            System.out.println("Host " + host + " is not reachable.");
        }
    }

    // @Test
    // public void testAccountAdd()throws Exception{
    //     String username = userManager.getNewUsername();
    //     Account account = new Account().setUsername(username).setEmail("123456@test.com");
    //     accountService.addAccount(account);
    //     try {
    //         Account account2 = accountService.findAccountByUsername(username);
    //         System.out.println(account2.getEmail());
    //     } catch (Exception e) {
    //         throw e;
    //     }
    // }

    @Autowired
    Firestore db;

    @Test
    public void testSimpleFirestore()throws Exception{
        // Create a Map to store the data we want to set
        Map<String, Object> docData = new HashMap<>();
        docData.put("name", "Los Angeles");
        docData.put("state", "CA");
        docData.put("country", "USA");
        docData.put("regions", Arrays.asList("west_coast", "socal"));
        // Add a new document (asynchronously) in collection "cities" with id "LA"
        ApiFuture<WriteResult> future = db.collection("cities").document("LA").set(docData);
        // ...
        // future.get() blocks on response
        System.out.println("Update time : " + future.get().getUpdateTime());
    }
}
