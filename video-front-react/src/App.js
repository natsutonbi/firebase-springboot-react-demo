import './App.css';
import React from 'react';
import { Link } from 'react-router-dom';


function App() {
  return (
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/login">登录</Link>
            </li>
            <li>
              <Link to="/upload">上传视频</Link>
            </li>
            <li>
              <Link to="/video">播放视频</Link>
            </li>
          </ul>
        </nav>
      </div>
  );
}

export default App;