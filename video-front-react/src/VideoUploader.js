import React, { useState } from 'react';
import 'firebase/storage';


import { getStorage, ref, uploadString, getDownloadURL } from 'firebase/storage';

// 初始化 Firebase App
// if (!firebase.apps.length) {
//     initializeApp(firebaseConfig);
// }

function VideoUploader() {
    const [videoUrl, setVideoUrl] = useState(null);

    const handleUpload = async (event) => {
        const file = event.target.files[0];
        const storage = getStorage();
        const storageRef = ref(storage, `videos/${file.name}`);
        await uploadString(storageRef, file, 'data_url');
        const url = await getDownloadURL(storageRef);
        setVideoUrl(url);
    };

    return (
        <div>
            <h2>Upload Video</h2>
            <input type="file" onChange={handleUpload} />
            {videoUrl && (
                <div>
                    <h2>Uploaded Video</h2>
                    <video controls width="400">
                        <source src={videoUrl} type="video/mp4" />
                        Your browser does not support the video tag.
                    </video>
                </div>
            )}
        </div>
    );
}

export default VideoUploader;